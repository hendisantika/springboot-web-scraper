package com.hendisantika.springbootwebscraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootWebScraperApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootWebScraperApplication.class, args);
    }

}
