package com.hendisantika.springbootwebscraper.service;

import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.Tag;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-06
 * Time: 07:52
 */
public interface TagService {

    Optional<Tag> findByTag(String tag);

    Collection<Tag> getTagsFromOtherUsersForLink(Link link);

    Collection<Tag> getTagsFromWebPageAnalysis(Link link);

    Collection<Tag> getTagsFromString(String string);
}