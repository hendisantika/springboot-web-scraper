package com.hendisantika.springbootwebscraper.service;

import com.hendisantika.springbootwebscraper.entity.User;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-06
 * Time: 07:53
 */
public interface UserService {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    User saveUser(User user);

}
