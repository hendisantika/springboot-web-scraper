package com.hendisantika.springbootwebscraper.service;

import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.User;
import com.hendisantika.springbootwebscraper.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-06
 * Time: 07:54
 */
@Service
public class LinkServiceImp implements LinkService {

    private final LinkRepository linkRepository;

    @Autowired
    public LinkServiceImp(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public Optional<Link> findLinkForId(Long id) {
        return linkRepository.findById(id);
    }

    @Override
    public Link saveLink(Link link) {
        return linkRepository.saveAndFlush(link);
    }

    @Override
    public Page<Link> findByUserOrderedByDatePageable(User user, Pageable pageable) {
        return linkRepository.findByUserOrderByCreateDateDesc(user, pageable);
    }

    @Override
    public Page<Link> findAllOrderedByDatePageable(Pageable pageable) {
        return linkRepository.findAllByOrderByCreateDateDesc(pageable);
    }

    @Override
    public void delete(Link link) {
        linkRepository.delete(link);
    }
}
