package com.hendisantika.springbootwebscraper.service;

import com.hendisantika.springbootwebscraper.entity.Link;
import com.hendisantika.springbootwebscraper.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-web-scraper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-06
 * Time: 07:52
 */
public interface LinkService {
    Optional<Link> findLinkForId(Long id);

    Link saveLink(Link link);

    /**
     * Finds a {@link Page) of {@link Link } of provided user ordered by date
     */
    Page<Link> findByUserOrderedByDatePageable(User user, Pageable pageable);

    /**
     * Finds a {@link Page) of all {@link Link } ordered by date
     */
    Page<Link> findAllOrderedByDatePageable(Pageable pageable);

    void delete(Link link);
}
